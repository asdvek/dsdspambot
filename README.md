### Setup on Ubuntu
1. Clone this repository to your local machine and `cd` to it.
```
git clone https://gitlab.com/asdvek/dsdspambot.git
cd dsdspambot
```

2. Install dependencies
```sudo apt install tesseract-ocr```

3. Setup python3 virtual environment and install python dependencies
```
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

4. [Create a Telegram bot and obtain its API token](https://core.telegram.org/bots#6-botfather)

5. Create a file with the name `secret.py`which defines the variable `api_token` to match the token of your bot.
```
api_token =  # your token here
```

6. Start the bot with `python3 main.py`