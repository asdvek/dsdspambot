import requests
from PIL import Image
import io


def fetch_image(url):
    """
    Download image to RAM from an image URL
    :param url: Image URL
    :return: PIL image on success, None on failure
    """
    response = requests.get(url)
    if response.status_code == 200:
        image_bytes = response.content
        im = Image.open(io.BytesIO(image_bytes))
        return im
    else:
        return None
