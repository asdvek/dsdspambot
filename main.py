import secret   # API token
import secrets
import random
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from utils.network import fetch_image
from PIL import Image
import pytesseract
import PIL.ImageOps


# TODO: proper logging to console


def main():
    updater = Updater(token=secret.api_token, use_context=True)
    dispatcher = updater.dispatcher

    # register command handlers
    blob_handler = CommandHandler('blob', blob)
    dispatcher.add_handler(blob_handler)

    # register message handler
    dsd_chant_handler = MessageHandler(Filters.text, inspect_text)
    dispatcher.add_handler(dsd_chant_handler)

    # register image handler
    image_handler = MessageHandler(Filters.photo, do_ocr)
    dispatcher.add_handler(image_handler)

    updater.start_polling()


# look for interesting contents in text messages
def inspect_text(update, context):
    chant_dsd(update, context, update.message.text)


# chant DSD if message_text mentions dsd in some way
def chant_dsd(update, context, message_text):
    if 'dsd' not in message_text.lower():
        return False

    chant_text = "DSD!!! "

    p_emoji = 0.5
    for i in range(3):
        if random.random() < p_emoji:
            chant_text += "🧨"
        if random.random() < p_emoji:
            chant_text += "💥"

    context.bot.send_message(chat_id=update.message.chat_id, text=chant_text)
    return True


# message handler that simply shows the received image for debugging purposes
def show_image(update, context):
    print("Received image!")
    image_id = update.message.photo[-1].file_id
    new_file = context.bot.get_file(image_id)
    image_url = new_file.file_path

    im = fetch_image(image_url)

    if im is not None:
        im.show()
    else:
        print("Could not fetch image from ", image_url)


def do_ocr(update, context):
    print("ocr_handler called")
    image_id = update.message.photo[-1].file_id
    new_file = context.bot.get_file(image_id)
    image_url = new_file.file_path

    print("Fetching image...")
    im = fetch_image(image_url)
    if im is None:
        print("TODO: retry on failed image downloads")
        return

    # preprocess image


    # do ocr
    print("Doing OCR...")

    print("Unprocessed image...")
    content = pytesseract.image_to_string(im)
    print(content)
    status = chant_dsd(update, context, content)
    if status is True:  # if dsd found ignore following tests
        return

    print("Inverted image...")
    inverted = PIL.ImageOps.invert(im)
    content = pytesseract.image_to_string(inverted)
    print(content)
    status = chant_dsd(update, context, content)
    if status is True:
        return

    print("ocr_handler exit")


def blob(update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text=str(secrets.token_bytes(64)))


if __name__ == '__main__':
    main()
